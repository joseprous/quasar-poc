class PlaceService {
  constructor() {
    this.places = [
      {
        id: 1,
        image: "https://picsum.photos/500/300?t=1",
        title: "Castillo Carlota Parmerola",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
      },
      {
        id: 2,
        image: "https://picsum.photos/500/300?t=2",
        title: "Playa de Aregua",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Penatibus et magnis dis parturient montes nascetur. ",
      },
      {
        id: 3,
        image: "https://picsum.photos/500/300?t=3",
        title: "Cerro Koi",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci dapibus ultrices in iaculis nunc sed augue.",
      },
      {
        id: 4,
        image: "https://picsum.photos/500/300?t=4",
        title: 'Almacén de Arte "El Cántaro"',
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt ornare massa eget egestas purus viverra accumsan in nisl. ",
      },
      {
        id: 5,
        image: "https://picsum.photos/500/300?t=5",
        title: "Escalinata de Areguá",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
      },
      {
        id: 6,
        image: "https://picsum.photos/500/300?t=6",
        title: 'Galería de Arte "Guggiari"',
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Penatibus et magnis dis parturient montes nascetur. ",
      },
      {
        id: 7,
        image: "https://picsum.photos/500/300?t=7",
        title: "Iglesia de Areguá",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci dapibus ultrices in iaculis nunc sed augue.",
      },
      {
        id: 8,
        image: "https://picsum.photos/500/300?t=8",
        title: "Estación de Ferrocarril",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt ornare massa eget egestas purus viverra accumsan in nisl. ",
      },
    ];
    this.categories = [
      { id: 1, label: "Museos", icon: "museum" },
      { id: 2, label: "Cerros", icon: "image" },
      { id: 3, label: "Playas", icon: "pool" },
      { id: 4, label: "Gastronomia", icon: "lunch_dining" },
      { id: 5, label: "Alojamiento", icon: "house" },
    ];
  }
  getCategories() {
    return this.categories;
  }
  getCategory(id) {
    return this.categories.find((x) => x.id === id);
  }
  getPlaces() {
    return this.places;
  }
  getPlace(id) {
    return this.places.find((x) => x.id === id);
  }
}

export default new PlaceService();
